#!/usr/bin/env node
import yargs from "yargs/yargs"
import { config } from "dotenv"
import { logMessage } from "./lib/utils/log"
import { provisionCustomFile } from "./lib/features/files/commands/provision"
import { pullCustomFile } from "./lib/features/files/commands/pull"
import { pushCustomFile } from "./lib/features/files/commands/push"

config()

const parser = yargs(process.argv.slice(2))
  .scriptName("files-manager")
  .command(
    "provision",
    "Provision file",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        name: { type: "string", alias: "n", demandOption: true },
      }),
    async (argv) => {
      await provisionCustomFile({
        appId: argv.appId,
        environment: argv.environment,
        fileName: argv.name,
      })
    }
  )
  .command(
    "pull",
    "Pull a file from the cloud",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        name: { type: "string", alias: "n", demandOption: true },
        outputFile: { type: "string", alias: "of", demandOption: true },
      }),
    async (argv) => {
      await pullCustomFile({
        outputFilePath: argv.outputFile,
        ref: {
          appId: argv.appId,
          environment: argv.environment,
          fileName: argv.name,
        },
      })
    }
  )
  .command(
    "push",
    "Push file to the cloud",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        name: { type: "string", alias: "n", demandOption: true },
        inputFile: { type: "string", alias: "if", demandOption: true },
      }),
    async (argv) => {
      await pushCustomFile({
        inputFilePath: argv.inputFile,
        ref: {
          appId: argv.appId,
          environment: argv.environment,
          fileName: argv.name,
        },
      })
    }
  )

parser.parse()
logMessage("Done ⭐")
