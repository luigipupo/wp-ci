export const replaceAll = (
  input: string,
  value: string,
  replaceWith: string
) => {
  return input.split(value).join(replaceWith)
}
