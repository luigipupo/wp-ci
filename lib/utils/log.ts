export const logBlankLine = () => console.log("")

export const logMessage = (message: string, data?: any) => {
  if (data) {
    console.log(message, data)
  } else {
    console.log(message)
  }
}

export const logError = (message: string, data?: any) => {
  if (data) {
    console.error(message, data)
  } else {
    console.error(message)
  }
}

export const logWarn = (message: string, data?: any) => {
  if (data) {
    console.warn(message, data)
  } else {
    console.warn(message)
  }
}

export const logElapsed = (message: string, start: Date, end: Date) => {
  const elapsed = end.getTime() - start.getTime()
  const minutes = Math.floor(elapsed / 60000)
  const seconds = ((elapsed % 60000) / 1000).toFixed(0)
  console.log(`${message} -> ${minutes}m ${seconds}s`)
}
