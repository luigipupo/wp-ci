import { existsSync, readFileSync } from "fs"

export const getContent = (filePath: string): string => {
  if (!existsSync(filePath)) {
    throw new Error(`File ${filePath} does not exist`)
  }
  return readFileSync(filePath, "utf8")
}
