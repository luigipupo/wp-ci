export interface AppFileRef {
  appId: string
  fileName: string
  environment: string
}
