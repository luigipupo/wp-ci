import {
  secretCreate,
  secretExists,
} from "../../../../infrastructure/aws/secrets-manager"
import { logMessage } from "../../../../utils/log"
import { buildEnvironmentTag } from "../../../secrets/utils/tags"
import { AppFileRef } from "../types"
import { buildFileSecretName } from "../utils"

export const provisionCustomFile = async (ref: AppFileRef) => {
  const fileSecretName = buildFileSecretName(ref)
  if (await secretExists(fileSecretName)) {
    logMessage(
      `🛑 File ${fileSecretName} already exists -> skipping initialization`
    )
    return
  }

  await secretCreate({
    name: fileSecretName,
    tags: [buildEnvironmentTag(ref.environment)],
  })
  logMessage(`🚀 Secret ${fileSecretName} provisioned`)
}
