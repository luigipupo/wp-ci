import { AppFileRef } from "../types"

const PATH_SEPARATOR = "/"
const FILES_DIRECTORY = "files"

export const buildFileSecretName = (ref: AppFileRef) =>
  [ref.appId, FILES_DIRECTORY, `${ref.environment}-${ref.fileName}`].join(
    PATH_SEPARATOR
  )
