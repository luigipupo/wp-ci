export interface SetAppSecretInput {
  appId: string
  environment: string
  service?: string
  key: string
  value: string
}
