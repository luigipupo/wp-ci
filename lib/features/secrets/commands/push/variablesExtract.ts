import { SecretValues } from "../../../../infrastructure/aws/secrets-manager"
import { jsonClone } from "../../../../utils/obj"
import {
  TokenizedFile,
  TokenizedVariable,
} from "../../utils/tokenization/tokenize"

export interface ProcessVariablesResult {
  serviceVariables: SecretValues
  environmentVariables: SecretValues
}

const processVariable = (
  variable: TokenizedVariable,
  {
    serviceVariables,
    environmentVariables,
  }: {
    serviceVariables: SecretValues
    environmentVariables: SecretValues
  }
) => {
  if (serviceVariables[variable.name]) {
    serviceVariables[variable.name] = variable.value
    return
  }

  if (environmentVariables[variable.name]) {
    environmentVariables[variable.name] = variable.value
    return
  }

  // if it is new variable, add it to service variables
  serviceVariables[variable.name] = variable.value
}

export interface ProcessVariablesResult {
  serviceVariables: SecretValues
  environmentVariables: SecretValues
}

export const processVariables = (
  tokenizedFile: TokenizedFile,
  { serviceVariables, environmentVariables }: ProcessVariablesResult
) => {
  const newSourceVariables = jsonClone(serviceVariables)
  const newEnvironmentVariables = jsonClone(environmentVariables)

  tokenizedFile.variables.forEach((x) =>
    processVariable(x, {
      environmentVariables: newEnvironmentVariables,
      serviceVariables: newSourceVariables,
    })
  )
  return {
    serviceVariables: newSourceVariables,
    environmentVariables: newEnvironmentVariables,
  }
}

export const createTokenizedFileWithoutVariables = (
  tokenizedFile: TokenizedFile
): TokenizedFile => {
  const templateFile = {
    ...tokenizedFile,
  }

  templateFile.variables.forEach((x) => (x.value = ""))
  return templateFile
}
