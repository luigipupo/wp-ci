import { TokenizedRowType } from "../../utils/tokenization/tokenize"
import { processVariables, ProcessVariablesResult } from "./variablesExtract"

describe("variablesExtract", () => {
  it("should append variables", () => {
    const result = processVariables(
      {
        rows: [
          { key: "KEY1", value: "${KEY1}", type: TokenizedRowType.Variable },
          { key: "KEY2", value: "${KEY2}", type: TokenizedRowType.Variable },
          { key: "", value: "", type: TokenizedRowType.Other },
          { key: "# AA BB CC", value: "", type: TokenizedRowType.Other },
          { key: "", value: "", type: TokenizedRowType.Other },
          { key: "KEY3", value: "${KEY3}", type: TokenizedRowType.Variable },
          { key: "KEY4", value: "${KEY4}", type: TokenizedRowType.Variable },
          { key: "    ", value: "", type: TokenizedRowType.Other },
        ],
        variables: [
          { name: "KEY1", value: "A" },
          { name: "KEY2", value: "B" },
          { name: "KEY3", value: "C" },
          { name: "KEY4", value: "D" },
          { name: "KEY5", value: "K" },
        ],
      },
      {
        serviceVariables: {
          KEY1: "E",
          KEY2: "F",
          KEY3: "G",
        },
        environmentVariables: {
          KEY2: "H",
          KEY4: "I",
        },
      }
    )

    expect(result).toEqual<ProcessVariablesResult>({
      serviceVariables: {
        KEY1: "A",
        KEY2: "B",
        KEY3: "C",
        KEY5: "K",
      },
      environmentVariables: {
        KEY2: "H",
        KEY4: "D",
      },
    })
  })
})
