import {
  secretCreate,
  secretExists,
} from "../../../../infrastructure/aws/secrets-manager"
import { logMessage } from "../../../../utils/log"
import { buildSecretPages } from "../../utils/pages"
import { buildEnvironmentTag } from "../../utils/tags"
import { AppSecretPage, AppSecretsRef } from "../types"

const provisionPage = async (page: AppSecretPage) => {
  if (await secretExists(page.name)) {
    logMessage(
      `🛑 Secret ${page.name} already exists -> skipping initialization`
    )
    return
  }

  await secretCreate({
    name: page.name,
    tags: [buildEnvironmentTag(page.environment)],
  })
  logMessage(`🚀 Secret ${page.name} provisioned`)
}

export const provisionAppSecrets = async (ref: AppSecretsRef) => {
  const pages = buildSecretPages(ref)
  for (const page of pages) {
    await provisionPage(page)
  }
}
