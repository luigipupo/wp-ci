import { SecretValues } from "../../../../infrastructure/aws/secrets-manager"
import {
  TokenizedFile,
  TokenizedVariable,
} from "../../utils/tokenization/tokenize"

const variableReplace = (
  variable: TokenizedVariable,
  {
    serviceVariables,
    environmentVariables,
  }: {
    serviceVariables: SecretValues
    environmentVariables: SecretValues
  }
) => {
  if (serviceVariables[variable.name]) {
    variable.value = serviceVariables[variable.name]
    return
  }

  if (environmentVariables[variable.name]) {
    variable.value = environmentVariables[variable.name]
    return
  }
}

export const variablesReplace = (
  tokenizedFile: TokenizedFile,
  {
    serviceVariables,
    environmentVariables,
  }: {
    serviceVariables: SecretValues
    environmentVariables: SecretValues
  }
): TokenizedFile => {
  const updatedFile = {
    ...tokenizedFile,
  }
  tokenizedFile.variables?.forEach((x) =>
    variableReplace(x, {
      serviceVariables,
      environmentVariables,
    })
  )
  return updatedFile
}
