import { writeFileSync } from "fs"
import {
  secretRawStringValueGet,
  secretValuesGet,
} from "../../../../infrastructure/aws/secrets-manager"
import { getSecretPageName } from "../../utils/pages"
import {
  parseTokenizedTemplateFile,
  stringifyEnvFile,
} from "../../utils/tokenization/tokenize"
import { AppSecretPageType } from "../types"
import { PullAppSecretInput } from "./types"
import { variablesReplace } from "./variablesReplace"
import { ensureDirectory, getDirectoryPath } from "../../../../utils/directory"

const getVariablePageNames = (input: PullAppSecretInput) => {
  const serviceTemplatePageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Template,
  })

  const serviceVariablesPageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Variables,
  })

  const envVariablesPageName = getSecretPageName({
    appId: input.appId,
    environment: input.environment,
    type: AppSecretPageType.Variables,
  })

  return {
    serviceTemplatePageName,
    serviceVariablesPageName,
    envVariablesPageName,
  }
}

const fetchSourceData = async (input: PullAppSecretInput) => {
  const pageNames = getVariablePageNames(input)

  const serviceVariables = await secretValuesGet({
    name: pageNames.serviceVariablesPageName,
  })
  const environmentVariables = await secretValuesGet({
    name: pageNames.envVariablesPageName,
  })

  const originalTemplateContent = await secretRawStringValueGet({
    name: pageNames.serviceTemplatePageName,
  })

  return {
    serviceVariables,
    environmentVariables,
    originalTemplateContent,
  }
}

export const pullSecretsFile = async (input: PullAppSecretInput) => {
  const { environmentVariables, originalTemplateContent, serviceVariables } =
    await fetchSourceData(input)

  // replace variables in template
  const envFile = variablesReplace(
    parseTokenizedTemplateFile(originalTemplateContent),
    {
      environmentVariables,
      serviceVariables,
    }
  )

  // write file
  const envFileContent = stringifyEnvFile(envFile)

  ensureDirectory(getDirectoryPath(input.outputFile))
  writeFileSync(input.outputFile, envFileContent, "utf8")
}
