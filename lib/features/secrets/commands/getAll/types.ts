export interface GetAppSecretPageInput {
  appId: string
  environment: string
  service?: string
}
