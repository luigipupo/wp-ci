import { secretRawStringValueGet } from "../../../../infrastructure/aws/secrets-manager"
import { getSecretPageName } from "../../utils/pages"
import { AppSecretPageType } from "../types"
import { GetAppSecretPageInput } from "./types"

export const getAppSecretPage = async (input: GetAppSecretPageInput) => {
  const pageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Variables,
  })
  return await secretRawStringValueGet({
    name: pageName,
  })
}
