import { SecretValues } from "../../../../infrastructure/aws/secrets-manager"
import { VariablesDict } from "../../types/variables"
import {
  parseEnvTemplateVariables,
  stringifyTemplateVariables,
} from "../../utils/parsing/template"

const mergeVariables = (dict: VariablesDict, secrets: SecretValues) => {
  Object.keys(dict).forEach((key) => {
    if (secrets[key] !== undefined) {
      dict[key] = secrets[key]
    }
  })
}

export const mergeTemplateFileVariables = ({
  templateContent,
  serviceVariables,
  environmentVariables,
}: {
  templateContent: string
  serviceVariables: SecretValues
  environmentVariables: SecretValues
}): string => {
  const variables = parseEnvTemplateVariables(templateContent)
  mergeVariables(variables, environmentVariables)
  mergeVariables(variables, serviceVariables)
  return stringifyTemplateVariables(variables)
}
