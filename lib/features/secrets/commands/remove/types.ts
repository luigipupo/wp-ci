export interface RemoveAppSecretInput {
  appId: string
  environment: string
  service?: string
  key: string
  value: string
}
