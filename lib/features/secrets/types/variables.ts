export interface VariablesDict {
  [key: string]: string | undefined
}
