import { Dict, toDict } from "../../../../utils/array"
import { toSingleValueMap } from "../../../../utils/maps"

export enum TokenizedRowType {
  Variable = "variable",
  Other = "other",
}

export interface TokenizedRow {
  key: string
  value: string
  type: TokenizedRowType
}

export interface TokenizedVariable {
  name: string
  value: string
}

export interface TokenizedFile {
  rows: TokenizedRow[]
  variables: TokenizedVariable[]
}

const COMMENT_TOKEN = "#"

const isCommentLine = (line: string): boolean =>
  line.trim().startsWith(COMMENT_TOKEN)

const isVariableLine = (line: string): boolean =>
  !isCommentLine(line) && line.trim().includes("=")

const createVariableToken = (key: string) => "${" + key.trim() + "}"

const parseRow = (
  line: string
): [TokenizedRow, TokenizedVariable | undefined] => {
  if (!isVariableLine(line)) {
    return [
      {
        key: line,
        value: "",
        type: TokenizedRowType.Other,
      },
      undefined,
    ]
  }
  const [key, value] = line.split("=")
  return [
    {
      key: key.trim(),
      value: createVariableToken(key.trim()),
      type: TokenizedRowType.Variable,
    },
    {
      name: key.trim(),
      value,
    },
  ]
}

const deduplicateVariables = (
  variables: TokenizedVariable[]
): TokenizedVariable[] => {
  const mapped = toSingleValueMap(
    variables,
    (x) => x.name,
    (x) => x.value
  )
  return Array.from(mapped.keys()).map((key) => ({
    name: key,
    value: mapped.get(key) as string,
  }))
}

export const tokenizeEnvFile = (content: string): TokenizedFile => {
  const lines = content.split("\n")
  const parsedRows = lines.map(parseRow)
  return {
    rows: parsedRows.map((x) => x[0]),
    variables: deduplicateVariables(
      parsedRows.map((x) => x[1] as TokenizedVariable).filter((x) => x)
    ),
  }
}

const formatVariableRow = (
  row: TokenizedRow,
  variables: Dict<TokenizedVariable>
) => [row.key, variables[createVariableToken(row.key)]?.value ?? ""].join("=")

const processRow = (row: TokenizedRow, variables: Dict<TokenizedVariable>) => {
  switch (row.type) {
    case TokenizedRowType.Variable:
      return formatVariableRow(row, variables)
    case TokenizedRowType.Other:
      return row.key
    default:
      throw new Error(`Unknown row type ${row.type}`)
  }
}

export const stringifyEnvFile = (file: TokenizedFile): string => {
  const variablesDict = toDict(file.variables ?? [], (x) =>
    createVariableToken(x.name)
  )
  const lines = file.rows?.map((row) => processRow(row, variablesDict)) ?? []
  return lines.join("\n")
}

export const stringifyTemplateFile = (rows: TokenizedFile) =>
  JSON.stringify(rows)

export const parseTokenizedTemplateFile = (content: string) =>
  JSON.parse(content) as TokenizedFile
