import { run } from "../../run"

interface Options {
  ecrPublic?: boolean
  whatIf?: boolean
}

export const ecrLogin = (options?: Options) => {
  run(
    `aws ${
      options?.ecrPublic ? "ecr-public" : "ecr"
    } get-login-password --region ${
      process.env.CI_AWS_ECR_REGION
    } | docker login --username ${
      process.env.CI_AWS_ECR_USERNAME
    } --password-stdin ${process.env.CI_AWS_ECR_PWD_STDIN}`,
    options?.whatIf
  )
}
