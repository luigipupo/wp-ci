import { run } from "./run"
import { logWarn } from "./utils/log"

export interface DockerCommandOptions {
  whatIf?: boolean
}

export const tagImage = (
  source: string,
  target: string,
  options?: DockerCommandOptions
) => run(`docker tag ${source} ${target}`, options?.whatIf)

export interface PullImageOptions extends DockerCommandOptions {
  throwIfNotFound?: boolean
}

export const pullImage = (image: string, options?: PullImageOptions) => {
  try {
    run(`docker pull ${image}`, options?.whatIf)
  } catch (e) {
    if (options?.throwIfNotFound) {
      throw e
    }
    logWarn(`Docker image not found -> ${image}`, e)
  }
}

export const pushImage = (image: string, options?: DockerCommandOptions) =>
  run(`docker push ${image}`, options?.whatIf)

export const showImageHistory = (
  image: string,
  options?: DockerCommandOptions
) => run(`docker history ${image}`, options?.whatIf)

export const lsImages = () => run(`docker image ls`)

export const getImageTag = (image: string) => {
  const parts = image.split(":")
  return parts[parts.length - 1]
}

export const buildImage = (
  input: {
    tag: string
    cacheFrom?: string[]
    workdir?: string
    target?: string
    dockerfile?: string
  },
  options?: DockerCommandOptions
) =>
  run(
    [
      "docker build",
      input.target ? `--target ${input.target}` : "",
      (input.cacheFrom?.length ?? 0) > 0
        ? input.cacheFrom?.map((x) => `--cache-from ${x}`).join(" ")
        : "",
      `-t ${input.tag}`,
      input.workdir ?? ".",
      input.dockerfile ? `-f ${input.dockerfile}` : "",
    ]
      .filter((x) => x)
      .join(" "),
    options?.whatIf
  )

export const runImageCommand = (
  input: {
    image: string
    command: string
    remove?: boolean
  },
  options?: DockerCommandOptions
) =>
  run(
    `docker run ${input.remove ? "--rm " : ""}${input.image} ${input.command}`,
    options?.whatIf
  )

export const copyImageFolder = (
  input: {
    image: string
    sourcePath: string
    targetPath: string
  },
  options?: DockerCommandOptions
) =>
  run(
    `docker cp ${input.image}:${input.sourcePath} ${input.targetPath}`,
    options?.whatIf
  )
