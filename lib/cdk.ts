import { run } from "./run"

interface Props {
  stack: string
  diff?: boolean
  deploy?: boolean
  bootstrap?: boolean
  args?: string
  whatIf?: boolean
}

export const cdkDeploy = ({
  stack,
  diff,
  deploy,
  bootstrap,
  args,
  whatIf,
}: Props) => {
  if (diff) {
    run(`yarn cdk diff ${stack}`, whatIf)
  }

  if (bootstrap) {
    run(`yarn cdk bootstrap --force`, whatIf)
  }

  if (deploy) {
    run(
      `yarn cdk deploy ${stack} ${args ?? ""} --require-approval never`,
      whatIf
    )
  }
}
