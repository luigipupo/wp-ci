#!/usr/bin/env node
import yargs from "yargs/yargs"
import { config } from "dotenv"
import { ecrLogin } from "./lib/infrastructure/aws/ecr"
import {
  buildImage,
  copyImageFolder,
  pullImage,
  pushImage,
  runImageCommand,
  showImageHistory,
  tagImage,
} from "./lib/docker"
import { dumpDotEnv } from "./lib/dot-env"
import { run } from "./lib/run"
import { getDockerImageName } from "./lib/infrastructure/docker/docker-registry"
import { logBlankLine, logElapsed, logMessage } from "./lib/utils/log"

const start = new Date()

config()

const argv = yargs(process.argv.slice(2)).options({
  workdir: { type: "string", alias: "wd" },
  image: { type: "string", alias: "svc", demandOption: true },
  environment: { type: "string", alias: "env", demandOption: true },
  stages: { type: "string", alias: "st" },
  stagesOnly: { type: "boolean" },
  build: { type: "boolean" },
  pull: { type: "boolean" },
  pullImage: { type: "string" },
  push: { type: "boolean" },
  pushStages: { type: "boolean" },
  pullStages: { type: "boolean" },
  noCache: { type: "boolean" },
  cacheFrom: { type: "string" },
  run: { type: "string" },
  dockerfile: { type: "string", alias: "df" },
  dumpEnv: { type: "string", alias: "de" },
  dumpEnvSource: { type: "string", alias: "deSrc" },
  dumpEnvTarget: { type: "string", alias: "deTarget" },
  dumpEnvAll: { type: "boolean" },
  copy: { type: "string", alias: "cp" },
  commitHash: {
    type: "string",
    default:
      process.env.CI_COMMIT_SHA ||
      (run("git rev-parse HEAD")?.replace("\n", "") as string),
  },
  whatIf: { type: "boolean" },
  ecrPublic: { type: "boolean" },
}).argv

const getBuildStages = (): string[] | undefined => {
  if (!argv.stages) {
    return undefined
  }
  return Array.isArray(argv.stages) ? argv.stages : [argv.stages]
}

const buildStages = getBuildStages()
const lastStageName = buildStages?.[buildStages.length - 1]

if (!process.env.CI_AWS_ECR_REGISTRY) {
  throw new Error("Missing docker registry")
}

if (argv.stagesOnly && !buildStages?.length) {
  throw new Error("buildStages must be specified when using stagesOnly")
}

const buildImageName = (suffix: string) =>
  getDockerImageName({
    environment: argv.environment,
    registry: process.env.CI_AWS_ECR_REGISTRY as string,
    image: argv.image,
    suffix,
  })

logMessage(`Commit hash ${argv.commitHash}`)
logBlankLine()

if (argv.dumpEnv) {
  logMessage("Dumping environment variables", argv.dumpEnv)
  dumpDotEnv({
    ...argv,
    dumpEnv: argv.dumpEnv,
  })
  logBlankLine()
}

const commitImageName = buildImageName(
  argv.stagesOnly ? `${lastStageName}-${argv.commitHash}` : argv.commitHash
)
const latestImageName = buildImageName(
  argv.stagesOnly ? `${lastStageName}-latest` : "latest"
)

logMessage("Authenticating to ECR")
ecrLogin({
  ecrPublic: argv.ecrPublic,
  whatIf: argv.whatIf,
})
logBlankLine()

if (argv.pullImage) {
  logMessage(`Pulling image ${argv.pullImage}`)
  pullImage(buildImageName(argv.pullImage), {
    whatIf: argv.whatIf,
    throwIfNotFound: false,
  })
  logBlankLine()
}

const stageImages = buildStages?.map((x) => buildImageName(x))
let lastStageImageName = ""

if (argv.build) {
  if (buildStages?.length) {
    for (const stage of buildStages) {
      const stageImageName = buildImageName(stage)
      const stageWithCommitImageName = buildImageName(
        `${stage}-${argv.commitHash}`
      )
      if (argv.pullStages) {
        logMessage("Pulling stage image", stageImageName)
        pullImage(stageImageName, {
          whatIf: argv.whatIf,
          throwIfNotFound: false,
        })
        logBlankLine()
      }

      logMessage(`Building stage ${stage} image`)
      buildImage(
        {
          cacheFrom: argv.noCache !== true ? stageImages : undefined,
          tag: stageWithCommitImageName,
          workdir: argv.workdir,
          dockerfile: argv.dockerfile,
          target: stage,
        },
        {
          whatIf: argv.whatIf,
        }
      )
      logBlankLine()

      if (argv.pushStages) {
        logMessage(`Tagging stage ${stage} image for push`)
        tagImage(stageWithCommitImageName, stageImageName, {
          whatIf: argv.whatIf,
        })
        logBlankLine()

        logMessage(`Pushing stage ${stage} image`)
        pushImage(stageImageName, {
          whatIf: argv.whatIf,
        })
        logBlankLine()
      }

      lastStageImageName = stageWithCommitImageName
    }
  }

  if (argv.stagesOnly) {
    if (lastStageImageName !== commitImageName) {
      logMessage(`Tagging latest stage image`)
      tagImage(lastStageImageName, commitImageName, {
        whatIf: argv.whatIf,
      })
      logBlankLine()
    }
  } else {
    if (argv.pull) {
      logMessage("Pulling latest image")
      pullImage(latestImageName, {
        whatIf: argv.whatIf,
        throwIfNotFound: false,
      })
      logBlankLine()
    }

    logMessage(`Building final image`)
    const defaultCacheFrom = argv.noCache !== true ? stageImages : undefined
    buildImage(
      {
        cacheFrom: argv.cacheFrom
          ? [buildImageName(argv.cacheFrom)]
          : defaultCacheFrom,
        tag: commitImageName,
        workdir: argv.workdir,
        dockerfile: argv.dockerfile,
      },
      {
        whatIf: argv.whatIf,
      }
    )
    logBlankLine()
  }
}

if (argv.run) {
  const commands = Array.isArray(argv.run) ? argv.run : [argv.run]
  for (const command of commands) {
    logMessage(`Running command ${command}`)
    runImageCommand(
      {
        image: commitImageName,
        command: command.trim(),
        remove: true,
      },
      {
        whatIf: argv.whatIf,
      }
    )
    logBlankLine()
  }
}

if (argv.copy) {
  const pathsList = Array.isArray(argv.copy) ? argv.copy : [argv.copy]
  for (const paths of pathsList) {
    logMessage(`Copying files ${paths}`)
    copyImageFolder(
      {
        image: commitImageName,
        sourcePath: paths.split(" ")[0],
        targetPath: paths.split(" ")[1],
      },
      {
        whatIf: argv.whatIf,
      }
    )
    logBlankLine()
  }
}

if (argv.push) {
  logMessage("Pushing commit image")
  pushImage(commitImageName, {
    whatIf: argv.whatIf,
  })
  logBlankLine()

  logMessage("Pushing latest image")
  tagImage(commitImageName, latestImageName, {
    whatIf: argv.whatIf,
  })
  pushImage(latestImageName, {
    whatIf: argv.whatIf,
  })
  logBlankLine()
}

logMessage("Image history ->", commitImageName)
showImageHistory(commitImageName, {
  whatIf: argv.whatIf,
})
logBlankLine()

logMessage("Image history ->", latestImageName)
showImageHistory(latestImageName, {
  whatIf: argv.whatIf,
})
logBlankLine()

const end = new Date()
logElapsed("Command time", start, end)
