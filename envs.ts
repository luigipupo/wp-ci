#!/usr/bin/env node
import yargs from "yargs/yargs"
import { config } from "dotenv"
import { provisionAppSecrets } from "./lib/features/secrets/commands/provision"
import { setAppSecret } from "./lib/features/secrets/commands/set"
import { getAppSecret } from "./lib/features/secrets/commands/get"
import { removeAppSecret } from "./lib/features/secrets/commands/remove"
import { composeSecretsFile } from "./lib/features/secrets/commands/compose"
import { pullSecretsFile } from "./lib/features/secrets/commands/pull"
import { pushSecretsFile } from "./lib/features/secrets/commands/push"
import { getAppSecretPage } from "./lib/features/secrets/commands/getAll"
import { logMessage } from "./lib/utils/log"

config()

const parser = yargs(process.argv.slice(2))
  .scriptName("envs-manager")
  .command(
    "provision",
    "Provision secrets",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        service: { type: "string", alias: "svc", demandOption: true },
      }),
    async (argv) => {
      await provisionAppSecrets(argv)
    }
  )
  .command(
    "set",
    "Set secret",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        service: { type: "string", alias: "svc" },
        key: { type: "string", alias: "k", demandOption: true },
        value: { type: "string", alias: "v", demandOption: true },
      }),
    async (argv) => {
      await setAppSecret(argv)
    }
  )
  .command(
    "get",
    "Get secret",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        service: { type: "string", alias: "svc" },
        key: { type: "string", alias: "k", demandOption: true },
      }),
    async (argv) => {
      const value = await getAppSecret(argv)
      logMessage(value)
    }
  )
  .command(
    "get-all",
    "Get secret page",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        service: { type: "string", alias: "svc" },
      }),
    async (argv) => {
      const value = await getAppSecretPage(argv)
      logMessage(JSON.stringify(JSON.parse(value), null, 2))
    }
  )
  .command(
    "remove",
    "Remove secret",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        service: { type: "string", alias: "svc" },
        key: { type: "string", alias: "k", demandOption: true },
      }),
    async (argv) => {
      await removeAppSecret(argv)
    }
  )
  .command(
    "pull",
    "Pulls .env file from the cloud",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        service: { type: "string", alias: "svc", demandOption: true },
        outputFile: { type: "string", alias: "of", demandOption: true },
      }),
    async (argv) => {
      await pullSecretsFile(argv)
    }
  )
  .command(
    "push",
    "Pulls .env file from the cloud",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        service: { type: "string", alias: "svc", demandOption: true },
        inputFile: { type: "string", alias: "if", demandOption: true },
      }),
    async (argv) => {
      await pushSecretsFile(argv)
    }
  )
  .command(
    "compose",
    "Compose a .env file using a local .env.template file and merging the secrets",
    (args) =>
      args.options({
        appId: { type: "string", alias: "app", demandOption: true },
        environment: { type: "string", alias: "env", demandOption: true },
        service: { type: "string", alias: "svc", demandOption: true },
        templateFile: { type: "string", alias: "tf", demandOption: true },
        outputFile: { type: "string", alias: "of", demandOption: true },
      }),
    async (argv) => {
      await composeSecretsFile(argv)
    }
  )

parser.parse()
logMessage("Done ⭐")
